import express, { Router } from 'express';
import { createHash } from 'crypto';

const transactionRoute: Router = express.Router();

transactionRoute.use(function(req, res, next) {
    console.log('%s %s %s', req.method, req.url, req.path);
    next();
});

transactionRoute
    .route('/')
    .get(function(req, res, next) {
        return res.status(200).send('handled');
    })
    .put(function(req, res, next) {
        return res.status(200).send('handled');
    })
    .post(function(req, res, next) {
        return res.status(200).send('handled');
    })
    .delete(function(req, res, next) {
        return res.status(200).send('handled');
    });

export default transactionRoute;