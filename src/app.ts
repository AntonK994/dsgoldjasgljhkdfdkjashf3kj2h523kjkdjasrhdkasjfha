import express from 'express';
import transactionRoute from '../endpoints/transaction';

start();

async function start() {
    const app = await getApp();
    return app.listen(3000);
}

export async function getApp() {
    const app = express();

    return app.use('/transaction', transactionRoute);
}